# SPDX-FileCopyrightText: 2022 Janet Blackquill <uhhadd@gmail.com>
#
# SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

find_package(Qt6Test REQUIRED)

add_executable(testNGLib
    test.cpp
)
add_test(testNGLib testNGLib)

target_link_libraries(
    testNGLib
        Qt6::Test NGLib
)
